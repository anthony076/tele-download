# https://pypi.org/project/progressbar2/

import asyncio, random
from pyrogram import Client

# create a new pyrogram-client
client = Client("an")
chat_id = "xingai8888"

async def main():

    await client.start()
    
    # ==== offset 的用法，從最新的位置往前移動 n 位 ====
    # 若最後的 id = 70063，offset = 3，取回的 id = latest - offset = 7063 - 3 = 7060 為起始
    # 若 limit = 5 ，則取回 7060 ~ 7056，最新的最先
    # async for message in client.iter_history(chat_id, limit=5, offset=3):
    #     print(message.message_id)        

    # ==== offset_id  的用法，移動到指定 ID ====
    # offset_id 會將指定的值，設置為 latest，
    # 例如，offset_id = 7050，即 latest = offset_id = 7050，pyrogram 會取回 7050 之後的 5 筆
    # 即 7049 ~ 7045
    # async for message in client.iter_history(chat_id, limit=5, offset_id=7050):
    #     print(message.message_id)  
    
    # ==== offset_date 的用法 ====
    # offset_date 需要輸入指定日期的 timestamp，返回指定日期以前的所有文章

    # 範例，搜尋某個日期的所有訊息
    # 例如，0517 = 1589731200, 0518 = 1589644800
    msgs = []
    startTimeStamp = 1589644800
    endTimeStamp = 1589904000
    async for message in client.iter_history(chat_id, offset_date=endTimeStamp):
        msgs.append(message.message_id)

    async for message in client.iter_history(chat_id, offset_date=startTimeStamp):
        if message.message_id in msgs:
            msgs.remove(message.message_id)

    print(msgs)
    await client.stop()

asyncio.get_event_loop().run_until_complete(main())