# https://blog.goodjack.tw/2020/04/create-datetime-with-timezone-via-python3-without-pytz.html

from datetime import datetime, timezone, timedelta

#datetime.datetime(年, 月, 日, 時, 分, 秒, 微秒, 時區)
t = datetime(2020, 5, 20, tzinfo=timezone(timedelta(hours=+8))).timestamp()
print(t)