import sys, hashlib, time, os
#save as md5_study.py
 
def md5(fileName, t):
    m = hashlib.md5()

    if t == "file":
        # 只讀取前 10% 的檔案內容
        size = int(os.stat(fileName).st_size * 0.1)

        with open(fileName,"rb", ) as fd:
            x = fd.read(size)

        with open("123"+fileName,"wb", ) as fw:
            fw.write(x)
            
    elif t == "name":
        x = fileName.encode("utf-8")
    else:
        assert False, "Unknown t"

    m.update(x)
    return m.hexdigest()

def exam(filename, t):
    start = time.time()
    print(md5(filename,t))
    cost = time.time()-start
    print(f"total cost: {cost}")


exam("test.mp4", t="file")
exam("ttt.mp4", t="file")

exam("test.mp4", t="name")
exam("ttt.mp4", t="name")

