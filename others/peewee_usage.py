
from peewee import *

db = SqliteDatabase('name.db')

# SELECT <要顯示的欄位> FROM <表格名>

class Base(Model):
    class Meta:
        database = db
        table_name = "mytable"

class Name (Base):
    filename = CharField(verbose_name="Original FileName", unique=True)
    b64 = CharField(verbose_name="encoded base64 string")

def add():
    Name.create(
        filename = "123",
        b64 = "456"
    ).save()

def singleFind():
    print("="*10)
    # ==== 單筆紀錄的查詢 ====
    # .get()取回單筆 <Model: Name> 的紀錄
    #   1.若不存在，會返回 __main__.NameDoesNotExist 的錯誤
    #   2.透過 Model.欄位，取得某一個欄位的值，例如，model.name
    #   3.若使用 print(Model)，會返回紀錄數
    record = Name.get(Name.filename == "這是測試.mp4").get()
    print(record)       #返回紀錄數
    print(record.b64)   #返回紀錄中的資料

def multiFind():
    print("="*10)

    # ==== 多筆紀錄的查詢 ====
    results = Name.select()
    
    for record in results:
        print(record.filename)

    # ==== 列印查詢到的紀錄數，只能用於多筆紀錄的查詢 ====
    results = Name.select(Name.filename).where(Name.filename=="這是測試1")
    print(results.count())

def toDict():
    print("="*10)
    # 將查詢結果轉換成集合類型
    results = [record.b64 for record in Name.select()]
    querys = Name.select().dicts()
    for row in querys:
        print(row)

def isExisted( filename):
    # 寫法1，透過 count()
    # if Name.select().where(Name.filename == filename).count() > 0:
    #     return True
    # else:
    #     return False

    # 寫法2，透過 exist()
    return  Name.select().where(Name.filename == filename).exists()



# Name.create_table([Name])
# singleFind()
# multiFind()
# toDict()

print(isExisted("這是文件1"))
print(isExisted("這是文件2"))
print(isExisted("這是文件3"))