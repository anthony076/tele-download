
import os
from peewee import IntegrityError
from asyncio import Queue, sleep, gather, get_event_loop
from pyrogram import Client
from pyrogram.types import Message
from hashlib import md5
from argparse import ArgumentParser
from typing import Optional

from helper.util import ProgressBar, genMD5Name, isMedia, \
    changeFileName, encryptDB, decryptDB, genMD5Name, \
    ProgressBar, scan, backupDB, containForeignWord, \
    removeFromProgressMap

# =======================
# init variables
# =======================

client = Client("an", config_file="config/config.ini")
client.workers = 10

chat_id = "self"
curGroupMediaId = 0
groupName = None
groupVideoCount = 1

isDebug = False

q: Queue = Queue()

# ==================
# define class
# ==================

class DownloadItem:
    def __init__(self, msgId, fileName, b64Name, md5Value):
        self.msgId = msgId
        self.fileName = fileName
        self.b64Name = b64Name
        self.md5Value = md5Value
        
        assert all([self.fileName != None, self.fileName != None, self.b64Name != None, self.md5Value != None]), "DownloadItem parameter error"

class Task:
    def __init__(self, downloadItem: DownloadItem):
        self.msgId = downloadItem.msgId
        self.fileName = downloadItem.fileName
        self.b64Name = downloadItem.b64Name
        self.md5Value = downloadItem.md5Value
    
    async def download(self) -> Optional[str]:
        #print(f"[download] msgId={self.msgId}, fileName={self.fileName}, b64Name={self.b64Name}, md5={self.md5Value}")

        msg = await client.get_messages(chat_id, self.msgId)

        # ! 注意，必須設置 block=True，下載才會進行
        result:Optional[str] = await client.download_media(
            block=True,
            message=msg, 
            file_ref=msg.video.file_ref,
            file_name=self.md5Value,
            progress=ProgressBar,
            progress_args = (self.msgId, )
        )

        return result

    async def run(self):
        try:
            # run download
            result:Optional[str] = await self.download()
            #print(f"[run] {self.msgId} is done.\n")

            # add to db
            if result == None:
                """ 下載失敗，result 為 None """
                
                # 將下載失敗的 msgId，從 ProgressMap 中移除
                # 下載成功的 msgId，會在 ProgressBar() 中自動被移除 (當進度達到 100%)，因此不需要手動移除
                removeFromProgressMap(self.msgId)
            else:
                """ 下載成功，result 為下載的路徑 """
                await handler.add(fileName=self.fileName, b64_str=self.b64Name, md5Value=self.md5Value)
                #print(f"[run] {self.msgId} is added to db\n")

        except IntegrityError as e:
            # ? IntegrityError，一般是寫入 db 錯誤，由於重複的檔名所造成
            #       當 message.caption 使用同一句廣告用語當作檔名時，就容易因檔名重複而造成 db 寫入錯誤
            #       例如，⑤更多视频请在tg收藏夹输入@AnchorPorn.mp4
            #       解決方式，發生錯誤時，嘗試改用 message.video.file_name 作為檔名
            # ! Bug，媒體群組訊息也有此問題，但不會報錯，因為有 count 值 加以區別，但無法從檔名識別影片
            # ? 若下載成功，但寫入 db 失敗，檔案依然是被保留的

            print(f"[run] IntegrityError {self.msgId} : {e}\n")
            msg = await client.get_messages(chat_id, self.msgId)
            print(f"[run] msg = {msg}")

            # step1，建立新名稱
            newFileName = msg.video.file_name
            newb64Name = DbHandler.str2b64(newFileName)
            newmd5Value = genMD5Name(newFileName.encode("utf-8"))

            # step2，將已建立的檔案進行更名
            os.rename(f"downloads/{self.md5Value}", f"downloads/{newFileName}")

            # step3，重新寫入 db 中
            await handler.add(fileName=newFileName, b64_str=newb64Name, md5Value=newmd5Value)
            
        except Exception as e:
            msg = await client.get_messages(chat_id, self.msgId)
            print(f"[run] Error {self.msgId} : {e}\n")
            print(f"[run] msg = {msg}")

# ==================
# main functions
# ==================

def setGroupName(message:Message):
    """ 
    設置媒體群組訊息的 groupName，用來當作檔名
    - 媒體群組訊息的第1片以後的 message ，不會有 message.caption，所以需要紀錄第1片的 message.caption 作為 groupName
    - 第1片的 message.caption 不一定總是有效，若無效，仍然可用分片自身的 message.video.file_name 作為檔名
    - groupName 和 fileName 的差異，groupName 是共同檔名，fileName 還要加上分集標記，避免檔案重名
    """
    gName = None

    if message.caption and containForeignWord(message.caption):
        """ PiorityA，message.caption 有效且包含外文 """ 
        gName = message.caption.split("\n")[0]

    elif message.video.file_name and containForeignWord(message.video.file_name):
        """ PiorityB，message.video.file_name 有效且包含外文 """ 
        gName = message.video.file_name

    elif message.caption:
        """ PiorityC，message.caption 有效但不包含外文 """
        gName = message.caption.split("\n")[0]
    
    elif message.video.file_name:
        """ PiorityD，message.video.file_name 包含外文 """ 
        gName = message.video.file_name

    else :
        """ PiorityE，message.caption 和 message.video.file_name 都無效 """
        gName = message.video.file_id
    
    return gName

def resetGroupParas():
    global groupName
    global groupVideoCount

    groupName = None
    groupVideoCount = 1

def getFileName(message):
    # ! bug，message.caption 有外文字，但是是重複出現的文字，造成寫入DB時出現 UNIQUE constraint failed: name.filename 的錯誤，例如，11486

    fileName = None

    global groupName
    global groupVideoCount
    global curGroupMediaId

    if isDebug:
        print("-"*30)
        print(message)

    # 判斷是否為"媒體群組訊息" 
    if message.media_group_id:
        """ 當前訊息為媒體群組訊息 """
        
        # ==== 判斷是否是同一個媒體群組訊息 ====
        # media_group_id 不同時，表示遇到連續且不同的媒體群組訊息，
        # 此時，重新紀錄當前 message.media_group_id，並重置相關參數
        if curGroupMediaId != message.media_group_id:
            curGroupMediaId = message.media_group_id
            resetGroupParas()

        # ==== 判斷是媒體群組訊息的第一片或是其他片 ====
        if groupName == None:
            """ 媒體群組訊息的第1片 """
            # 設置 groupName
            groupName = setGroupName(message)
            # 設置 fileName
            fileName = groupName + f"-{groupVideoCount}"
        else:
            """ 媒體群組訊息的其他分片 """
            fileName = groupName + f"-{groupVideoCount}"

        groupVideoCount += 1

    else:
        """ 當前訊息為一般訊息 """

        # 判斷是否需要清理媒體群組訊息的相關參數
        if groupName:
            """
            沒有 message.media_group_id，但有 groupName，
            - 剛離開媒體群組訊息後的第一則一般訊息，需要清除媒體群組訊息所設置的相關參數
            - 當前訊息沒有 media_group_id 但是有 groupName，表示上一個訊息為媒體群組訊息，但當前為一般訊息
            """
            resetGroupParas()

        fileName = setGroupName(message)

    assert fileName, "fileName is None"

    return fileName

async def delMsg (msgId: int):
    result = await client.delete_messages(
        chat_id=chat_id,
        message_ids=msgId,
        revoke=False
    )

    print(result )
    
    if result == True:
        print(f"[delMsg] {msgId} deleted \n")

async def getMaxMsgId() -> int:
    msgs = await client.get_history(chat_id, limit=1)
    return msgs[0]["message_id"]
    
async def getMinMsgId() -> int:
    msgs = await client.get_history(chat_id, limit=1, reverse=True)
    return msgs[0]["message_id"]

async def worker(workerId:int):
    """
    get downloadItem from q -> create Task -> execute Task.run()
    """
    
    while True:
        if not q.empty():
            # get downloadItem from queue
            item: DownloadItem = await q.get()
            #print(f"[worker-{workerId}] {item.msgId} is downloading ...\n")
            
            # create task instance
            task:Task = Task(downloadItem=item)
            
            try:
                # execute download
                await task.run()
            except Exception as e:
                print(f"[worker-{workerId}] Error : {e}")
            
        else:
            pass

        await sleep(1)

async def checkDownload():
    """ 
    檢查是否下載，若是，將 msgId 添加到 queue 中
    """

    # 取得 msgId 的最大值和最小值
    maxMsgId = await getMaxMsgId()
    minMsgId = await getMinMsgId()

    # ! 注意，msgId 最小從1開始，msgId=0，會出現 No argument supplied. Either pass message_ids or reply_to_message_ids
    msgId = args.n if args.n != None else minMsgId
    historyCount = await client.get_history_count(chat_id)

    print(f"historyCount={historyCount}")
    print(f"maxMsgId={maxMsgId}")
    print(f"minMsgId={minMsgId}")
    print(f"MsgId={msgId}")
    print()
    print("[check] checking")

    while True:
        if msgId == maxMsgId :
            """ 所有 msg 已檢查完畢，重新設置 msgId 和 maxMsgId，檢查是否有新的 msg """
            msgId = args.n
            maxMsgId = await getMaxMsgId()

        if q.qsize() <= 6 :
            """ 若 queue-size <= 6，持續檢查 msg 是否下載 """ 

            msg:Message = await client.get_messages(chat_id, msgId)
            
            # ==== step1，過濾空訊息或非媒體訊息 =====
            if (msg["empty"] == True) or (not isMedia(msg)):
                """ 當訊息為空，或不是媒體訊息時 """
                # ! 訊息為空時，在tele 中該訊息已刪除，不需要另外刪除

                msgId += 1
                #print(f"[check] {msgId} is not media \n")
                continue
            else:
                fileName = getFileName(msg)
                b64Name = DbHandler.str2b64(fileName)
                md5Value = genMD5Name(fileName.encode("utf-8"))

            # ==== step2，判斷是否要進行下載: 必須 db 無紀錄 ====
            if not handler._isExisted(md5Value):
                """ new coming mdeia message """
                #print(f"[check] add {msgId} into queue ...\n") 
                q.put_nowait(DownloadItem(msgId=msgId, fileName=fileName, b64Name=b64Name, md5Value=md5Value))
            else :
                """ old mdeia message, do nothing """
                #print(f"[check] ignore {msgId} ...\n")
                pass
            
            msgId += 1

            # TODO，添加對本地端檔案的判斷，若本地端有檔案，但DB無紀錄，僅添加到 db，不進行下載

        else : 
            """ 若 queue-size > 6，進入等待，避免佔用太多記憶體 """  
            pass

        await sleep(0.1)

async def main():
    await client.start()

    await gather(
        checkDownload(),
        worker(1),
        worker(2),
        worker(3),
        worker(4),
        worker(5),
        return_exceptions=False
    )

    await client.stop()

if __name__ == "__main__":
    parser = ArgumentParser(usage="python ttt.py --help")
    
    # action="store_true" 純開關，代表不需要參數
    parser.add_argument("-r","--run", action="store_true", help="run downlaod")
    parser.add_argument("-n","--n", type=int, help="set start msgId, default --s=1")
    parser.add_argument("-d","--decrypt", action="store_true", help="reverse videoName from db")
    parser.add_argument("-e","--encrypt", action="store_true", help="encrypt videoName to secret")
    parser.add_argument("-l","--lock", action="store_true", help="lock db, and produce encryptdb.db")
    parser.add_argument("-u","--unlock", action="store_true", help="unlock db, and produce tele.db")
    parser.add_argument("-s","--scan", action="store_true", help="unlock db, and produce tele.db")
    parser.add_argument("-b","--backup", action="store_true", help="backup db to cloud")
    
    args = parser.parse_args()

    if args.run:
        from helper.handler import DbHandler, Name
        handler = DbHandler(Name)

        try:
            #asyncio.get_event_loop().run_until_complete(main())
            get_event_loop().run_until_complete(main())
        except Exception as e:
            client.stop()
            print(f"{e}\n")
        
    elif args.decrypt:
        changeFileName(mode="decrypt")
    elif args.encrypt:
        changeFileName(mode="encrypt")
    elif args.lock:
        encryptDB(source="tele.db", dist="encrypted.db")
    elif args.unlock:
        decryptDB(source="encrypted.db", dist="tele.db")
    elif args.scan:
        scan()
    elif args.backup:
        backupDB()
    else:
        print()
        print("No command found, Use `python main.py --help` for detail usage")
        print()
    