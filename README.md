## Usage
- `python main.py --run` 或 `python main.py -r`
    run download script with default configure

- `python main.py --run --n 500` 或 `python main.py -rn 500`
    run download script, and set start-message-id to 5

- `python main.py --encrypt` 或 `python main.py -e`
    encrypt video name for videos inside downloads folder

- `python main.py --decrypt` 或 `python main.py -d`
    decrypt video name for videos inside downloads folder

## Message
- message 的 message_id 是唯一且固定的，即使訊息被刪除，message_id 依舊會被保留，但內容會被清空，此時，message["empty"] = true
  注意，message_id 不一定等於 get_history_count() 的返回值，

## 會出現檔名的地方
- message.video.file_name，另存新檔的時候使用

- message.caption，顯示在媒體的下方，
    - 注意，若是一個貼子裡面有多個影片，會共用同一個 message.caption，且只會顯示在第一個 message 中，

    - 可以透過 message.media_group_id 來判斷，統一個貼子的所有 message 會使用同一個media_group_id，
    且同一個貼子多個影片才會有 media_group_id，因此，`message 必須是順序下載，才能控制影片的檔名`

    - 有 media_group_id 時，把 message.caption 記錄下來，下一個 message 也使用同一個檔名，
    並加上 index 加以區別不同的影片

## progress 參數的使用，
https://docs.pyrogram.org/api/methods/download_media?highlight=download_media#pyrogram.Client.download_media

## TODOs
- fix，不用使用副檔名，使用 download 資料夾就不需要附檔名
- fix，利用 db 判斷是否需要下載，避免每次都要移動影片
- fix，修改 name.db -> tele.db
- fix，修改 db 內容，去掉 *.mp4
- 新增 db 加解密
- 備份檔案 https://iterative.github.io/PyDrive2/docs/build/html/filemanagement.html