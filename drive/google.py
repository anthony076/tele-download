import re
from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive

class MyDrive:
    settingFile = "config/settings.yaml"
    folder_id:str = None
    db_id:str = None

    def __init__(self):
        self._drive:GoogleDrive = None

    @property
    def drive(self):
        return self._drive

    def get_drive(self):
        gauth = GoogleAuth(settings_file=MyDrive.settingFile)
        gauth.LocalWebserverAuth()
        self._drive = GoogleDrive(gauth)
        return self
    
    def folder(self, folderName:str):
        file_list = self._drive.ListFile({"q": "'root' in parents and trashed=false"}).GetList()
        
        for f in file_list:
            if f['title'] == folderName : MyDrive.folder_id = f["id"]
        
        return self

    def db(self, dbName:str):
        createdDate:int = 0

        file_list = self._drive.ListFile({"q": f"'{MyDrive.folder_id}' in parents and trashed=false"}).GetList()
        
        for f in file_list:
            name = f["title"]
            d = int("".join(re.findall(r'\d+', name)))

            if (dbName in name) & (d > createdDate) :
                MyDrive.db_id = f["id"]
                createdDate = d

        return self