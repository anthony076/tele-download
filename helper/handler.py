# 注意，資料庫若新增欄位，則peewee的定義表也要添加對應的欄位，否則就算有提供值，該欄位也會寫入null，因為未定義

from base64 import b64encode, b64decode
from peewee import *
import os,re

from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
from drive.google import MyDrive

db = SqliteDatabase('tele.db')

def check_db_file():
    drive = MyDrive().get_drive().folder("db").db("tele").drive

    if not os.path.isfile("tele.db"):
        # 指定要下載的檔案
        targetFile = drive.CreateFile({'id': f"{MyDrive.db_id}"})
        targetFile.GetContentFile('tele.db') 

class Base(Model):
    class Meta:
        database = db

class Name (Base):
    filename = CharField(verbose_name="Original FileName", unique=True)
    b64 = CharField(verbose_name="encoded base64 string")
    md5 = CharField(verbose_name="for file search")

class DbHandler:
    def __init__(self, model:Name):
        self.model = model
        self.model.create_table([self.model])

    @staticmethod
    def str2b64(filename:str) -> str:
        return b64encode(filename.encode("utf-8")).decode("utf-8").replace("/", "#")

    def getStr_from_md5(self, md5:str) -> str:
        return self.model.select(self.model.filename).where(self.model.md5==md5).get().filename

    def getMd5_from_str(self, filename:str) -> str:
        return self.model.select(self.model.md5).where(self.model.filename==filename).get().md5

    def getMd5_from_b64(self, b64:str) -> str:
        return self.model.select(self.model.md5).where(self.model.b64==b64).get().md5

    def _isExisted(self, md5:str) -> bool:
        return self.model.select().where(self.model.md5==md5).exists()

    async def add(self, fileName:str, b64_str:str, md5Value:str) -> None:
        # == 檢查紀錄是否已經存在 ==
        if (not self._isExisted(fileName)):
            self.model.create(
                filename = fileName,
                b64 = b64_str,
                md5 = md5Value
            ).save()
        else:
            print("record is existed !!")

    def add_sync(self, fileName:str, b64_str:str, md5Value:str) -> None:
        # == 檢查紀錄是否已經存在 ==
        if (not self._isExisted(fileName)):
            self.model.create(
                filename = fileName,
                b64 = b64_str,
                md5 = md5Value
            ).save()
        else:
            print("record is existed !!")

check_db_file()
