import os
import datetime
from glob import glob
from base64 import b64encode, b64decode
from hashlib import md5
from drive.google import MyDrive
from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
from pyrogram.types import Message

from colorama import init, Fore, Back, Style
init()

from helper.handler import DbHandler, Name

db = DbHandler(Name)
downloadFiles:list = [ path.split("\\")[1] for path in glob("downloads/*")]
progressMap = {}

def changeFileName(mode):
    dFolder = "downloads/"
    assert os.path.isdir(dFolder), "downloads folder is not exist"

    # 取得所有 mp4 類型檔案
    fileList = [file for file in os.listdir(dFolder)]
    
    for sourceFile in fileList:        
        if mode == "encrypt":
            # 原始檔名 123.mp4 換成 MD5
            sFileName = sourceFile.replace(".mp4", "") 
            #distFile = b64encode(sFileName.encode("utf-8")).decode("utf-8").replace("/", "#")
            distFile = db.getMd5_from_str(sFileName)

        elif mode == "decrypt":
            # 將 MD5 檔名，替換成 123.mp4
            #sFileName = sourceFile.replace("#", "/")
            #distFile = b64decode(sFileName.encode("utf-8")).decode("utf-8")
            sFileName = sourceFile
            distFile = db.getStr_from_md5(sFileName)
            
            # strip invalid charater
            distFile = distFile.replace(":", "").replace("/", "").replace("?", "").replace(">", "").replace("\"", "")
            distFile += ".mp4"
        else:
            assert False, "Unknown mode"

        # 為檔案添加當前資料夾路徑
        distFile = dFolder + distFile
        sourceFile = dFolder + sourceFile 

        os.rename(sourceFile, distFile)

def scan():
    """
    Scan download folder. 
    If the file is in download folder but not recorded in db, then add file into db.
    """
    for fileName in downloadFiles:
        if not db._isExisted(fileName):
            print("="*30)
            
            b64 = DbHandler.str2b64(fileName)
            md5 = genMD5Name(fileName.encode("utf-8"))
            db.add_sync(fileName, b64, md5)

            print(f"add {fileName} into db ...")

def genMD5Name(s):
    m=md5()
    m.update(s)
    return m.hexdigest()

def encryptDB(source, dist):
    pass
    # assert os.path.isfile(source), f"{source} is not exist."

    # conn = sqlite.connect(source)
    # cursor = conn.cursor()

    # q = f'''ATTACH DATABASE '{dist}' AS encrypted KEY 'ching';\
    #         SELECT sqlcipher_export('encrypted');\
    #         DETACH DATABASE encrypted;'''
    # cursor.executescript(q)

    # conn.commit()
    # conn.close()

    # os.remove("tele.db")

def decryptDB(source, dist):
    pass
    # assert os.path.isfile(source), f"{source} is not exist."
    
    # conn = sqlite.connect(source)
    # cursor = conn.cursor()

    # cursor.execute("PRAGMA key='ching';")
    # cursor.execute(f"ATTACH DATABASE '{dist}' AS tele KEY '';")
    # cursor.execute("SELECT sqlcipher_export('tele');")
    # cursor.execute("DETACH DATABASE tele;")

    # conn.commit()
    # conn.close()

    # os.remove("encrypted.db")

def removeFromProgressMap(msgId:int):
    del progressMap[msgId]

async def ProgressBar (current, total, msgId):  
    progress = ""

    if progressMap.get(msgId) == None:
        """ 第一次調用，添加到 progressMap 中 """ 
        progressMap[msgId] = 0
    else: 
        """ 非第一次調用，更新當前 msgId 的 percent 數值 """ 
        percent:str = ("{0:." + str(1) + "f}").format(100 * (current / float(total))) 
        progressMap[msgId] = percent

    # 印出所有正在下載中的任務進度
    # 注意，print(end="\r") 為輸出不換行，end="\r" 才能直接更新數值而不移動游標
    # 缺點，斷行後，原本輸出的位置會被取代
    for k, v in sorted(progressMap.items()):
        #progress += f"[{k}] {v}%  "
        progress += Fore.CYAN + f'[{k}] ' + Fore.WHITE + f"{v}%  "

    print(progress, end="\r")

    if current == total:
        removeFromProgressMap(msgId)
        # 產生斷行，使下一個訊息能夠輸出
        print()

def backupDB():
    drive:GoogleDrive = MyDrive().get_drive().folder("db").db("tele").drive

    # 建立 file，並指定要上傳的目錄
    df1 = drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": MyDrive.folder_id}]})

    # 指定要上傳的檔案
    df1.SetContentFile('tele.db')
    
    # 取得檔案名稱
    datetime_dt = datetime.datetime.today()# 獲得當地時間
    datetime_str = datetime_dt.strftime("%Y%m%d_%H%M%S")  # 格式化日期

    # 變更 drive 保存檔案的名稱
    df1["title"] = f"tele-{datetime_str}.db"

    # 執行上傳
    df1.Upload()

def isMedia(message:Message) -> bool:
    if (message.media == True) and (message.video != None):
        return True
    else:
        return False

def containForeignWord(word:str) -> bool:
    """ 判斷檔名是否包含中文、日文、韓文 """
    for char in word:
        # zh
        if "\u4e00" <= char <= '\u9fa5':
            return True

        # kr
        if "\u3130" <= char <= '\u318F':
            return True

        # kr
        if "\uAC00" <= char <= '\uD7A3':
            return True

        # jp
        if "\u0800" <= char <= '\u4e00':
            return True

    return False